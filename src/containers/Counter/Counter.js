import React, {useEffect} from 'react';
import './Counter.css';
import {useDispatch, useSelector} from "react-redux";
import {add, decrement, fetchCounter, increment, saveCounter, subtract} from "../store/actions";

const Counter = () => {
    const dispatch = useDispatch();
    const state = useSelector(state => state);
    const increaseCounter = () => dispatch(increment());
    const decreaseCounter = () => dispatch(decrement());
    const plusCounter = () => dispatch(add(5));
    const minusCounter = () => dispatch(subtract(5));

    useEffect(() => {
        dispatch(fetchCounter());
    }, [dispatch]);

    useEffect(() => {
        dispatch(saveCounter(state.counter));
    }, [dispatch, state.counter]);

    return (
        <div className="Counter">
            <h1>{state.counter}</h1>
            <button className="Counter-button" onClick={increaseCounter}>Increase</button>
            <button className="Counter-button" onClick={decreaseCounter}>Decrease</button>
            <button className="Counter-button" type="submit" onClick={plusCounter}>Increase by 5</button>
            <button className="Counter-button" type="submit" onClick={minusCounter}>Decrease by 5</button>
        </div>
    );
};

export default Counter;