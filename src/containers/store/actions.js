import axios from "axios";

export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';
export const ADD = 'ADD';
export const SUBTRACT = 'SUBTRACT';

export const FETCH_COUNTER_REQUEST = 'FETCH_COUNTER_REQUEST';
export const FETCH_COUNTER_SUCCESS = 'FETCH_COUNTER_SUCCESS';
export const FETCH_COUNTER_FAILURE = 'FETCH_COUNTER_FAILURE';

export const SAVE_COUNTER = 'SAVE_COUNTER';

export const increment = () => ({type: INCREMENT});
export const decrement = () => ({type: DECREMENT});
export const add = value => ({type: ADD, value});
export const subtract = value => ({type: SUBTRACT, value});

export const fetchCounterRequest = () => ({type: FETCH_COUNTER_REQUEST});
export const fetchCounterSuccess = counter => ({type: FETCH_COUNTER_SUCCESS, counter});
export const fetchCounterFailure = () => ({type: FETCH_COUNTER_FAILURE});

export const saveCounterType = () => ({type: SAVE_COUNTER});

export const fetchCounter = () => {
    return async dispatch => {
        dispatch(fetchCounterRequest());

        try {
            const response = await axios.get('https://propject-burger-ponamareva-default-rtdb.firebaseio.com/counter.json');

            dispatch(fetchCounterSuccess(response.data));
        } catch (e) {
            dispatch(fetchCounterFailure());
        }
    }
};

export const saveCounter = data => {
    return async dispatch => {
        try {
            console.log(data);
            await axios.put('https://propject-burger-ponamareva-default-rtdb.firebaseio.com/counter.json', data);
            dispatch(saveCounterType());
        } catch (e) {
            console.log(e);
        }
    }
};
